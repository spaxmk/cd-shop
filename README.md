Web aplikacija za katalog muzičkih albuma korišćenjem ASP .NET MVC Framework-a rađena u svrhu završnog rada.

Završni rad objašnjava postupak izrade web aplikacije korišćenjem ASP .NET MVC Framework-a,
kao i tehnologija koje su korišćene za izradu. Korišćenjem ASP .NET MVC Framework-a izgrađena je
web aplikacija za katalog muzičkih albuma.

Aplikacija predstavlja servis koji treba da omogući tabelarni prikaz svih muzičkih albuma koji se
nalaze u bazi podataka, fukcionalnosti dodavanje novih albuma, izmenu, brisanje i informaciju u
kojoj se prodavnici nalaze albumi.
Pomoću Bootstrap-a i JQuery potrebno je definisati izgled aplikacije. Rad treba da pokaže na koji
način se može napraviti aplikacija korišćenjem ASP .NET MVC Framework-a.
Cilj aplikacije je olakšavanje rada korisniku prilikom vođenja evidencije o stanju albuma koji su
dosupni