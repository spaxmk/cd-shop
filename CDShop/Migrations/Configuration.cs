﻿namespace CDShop.Migrations
{
    using CDShop.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CDShop.Models.CDstoreContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CDShop.Models.CDstoreContext context)
        {
            context.Musicstores.AddOrUpdate(
                new Musicstore() { Id = 1, Name = "Metropolis Music", City = "Beograd" },
                new Musicstore() { Id = 2, Name = "Mitros Music", City = "Novi Sad" },
                new Musicstore() { Id = 3, Name = "Mascom", City = "Beograd" },
                new Musicstore() { Id = 4, Name = "Multimedia Music", City = "Beograd" },
                new Musicstore() { Id = 5, Name = "Music Box", City = "Sabac" },
                new Musicstore() { Id = 6, Name = "MediaWeb p.r.", City = "Beograd" }
                );

            context.SaveChanges();
            
        }
    }
}
