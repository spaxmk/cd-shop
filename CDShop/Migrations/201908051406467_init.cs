﻿namespace CDShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Musics",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Author = c.String(nullable: false, maxLength: 50),
                        genre = c.Int(nullable: false),
                        Price = c.Double(nullable: false),
                        Created = c.DateTime(nullable: false),
                        MusicstoreId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Musicstores", t => t.MusicstoreId, cascadeDelete: true)
                .Index(t => t.MusicstoreId);
            
            CreateTable(
                "dbo.Musicstores",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        City = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Musics", "MusicstoreId", "dbo.Musicstores");
            DropIndex("dbo.Musics", new[] { "MusicstoreId" });
            DropTable("dbo.Musicstores");
            DropTable("dbo.Musics");
        }
    }
}
