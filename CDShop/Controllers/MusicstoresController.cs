﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CDShop.Models;

namespace CDShop.Controllers
{
    public class MusicstoresController : Controller
    {
        private CDstoreContext db = new CDstoreContext();

        // GET: Musicstores
        public ActionResult Index()
        {
            return View(db.Musicstores.ToList());
        }

        // GET: Musicstores/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Musicstore musicstore = db.Musicstores.Find(id);
            if (musicstore == null)
            {
                return HttpNotFound();
            }
            return View(musicstore);
        }

        // GET: Musicstores/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Musicstores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,City")] Musicstore musicstore)
        {
            if (ModelState.IsValid)
            {
                db.Musicstores.Add(musicstore);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(musicstore);
        }

        // GET: Musicstores/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Musicstore musicstore = db.Musicstores.Find(id);
            if (musicstore == null)
            {
                return HttpNotFound();
            }
            return View(musicstore);
        }

        // POST: Musicstores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,City")] Musicstore musicstore)
        {
            if (ModelState.IsValid)
            {
                db.Entry(musicstore).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(musicstore);
        }

        // GET: Musicstores/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Musicstore musicstore = db.Musicstores.Find(id);
            if (musicstore == null)
            {
                return HttpNotFound();
            }
            return View(musicstore);
        }

        // POST: Musicstores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Musicstore musicstore = db.Musicstores.Find(id);
            db.Musicstores.Remove(musicstore);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
