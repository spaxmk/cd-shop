﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using CDShop.Models;
using CDShop.Models.Filter;
using Microsoft.Ajax.Utilities;
using PagedList;


namespace CDShop.Controllers
{
    public class MusicsController : Controller
    {
        private CDstoreContext db = new CDstoreContext();

        public enum Sorting
        {
            Name,
            Author,
            Price
        }

        private static Dictionary<string, Sorting> sortDic = new Dictionary<string, Sorting>()
        {
            {"Name", Sorting.Name },
            {"Author", Sorting.Author },
            {"Price", Sorting.Price }
        };

        SelectList list = new SelectList(sortDic, "Key", "Key", "Name");

        private int pageSize = 8;

        // GET: Musics
        public ActionResult Index(MusicFilter filter, string sortType = "Name", int pageNum = 1)
        {
            IEnumerable<Music> allMusic = db.Musics.Include(m => m.Musicstore);

            Sorting sortBy = sortDic[sortType];

            switch (sortBy)
            {
                case Sorting.Name:
                    allMusic = allMusic.OrderBy(b => b.Name);
                    break;
                case Sorting.Author:
                    allMusic = allMusic.OrderBy(b => b.Author);
                    break;
                case Sorting.Price:
                    allMusic = allMusic.OrderBy(b => b.Price);
                    break;
            }

            if (sortType == "")
            {
                sortType = "Name";
            }
            if (!filter.Name.IsNullOrWhiteSpace())
            {
                allMusic = allMusic.Where(f => f.Name.Contains(filter.Name));
            }
            if (!filter.Author.IsNullOrWhiteSpace())
            {
                allMusic = allMusic.Where(f => f.Author.Contains(filter.Author));
            }
            if (filter.Price != 0.00)
            {
                allMusic = allMusic.Where(f => f.Price == filter.Price);
            }
            ViewBag.CurrentFilter = filter;
            ViewBag.CurrentSortType = sortType;
            ViewBag.Sorting = list;
            return View(allMusic.ToPagedList(pageNum, pageSize));


            //var musics = db.Musics.Include(m => m.Musicstore);
            //return View(await musics.ToListAsync());
        }

        // GET: Musics/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Music music = await db.Musics.FindAsync(id);
            if (music == null)
            {
                return HttpNotFound();
            }
            return View(music);
        }

        // GET: Musics/Create
        public ActionResult Create()
        {
            ViewBag.MusicstoreId = new SelectList(db.Musicstores, "Id", "Name");
            return View();
        }

        // POST: Musics/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Author,genre,Price,Created,MusicstoreId")] Music music)
        {
            if (ModelState.IsValid)
            {
                db.Musics.Add(music);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.MusicstoreId = new SelectList(db.Musicstores, "Id", "Name", music.MusicstoreId);
            return View(music);
        }

        // GET: Musics/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Music music = await db.Musics.FindAsync(id);
            if (music == null)
            {
                return HttpNotFound();
            }
            ViewBag.MusicstoreId = new SelectList(db.Musicstores, "Id", "Name", music.MusicstoreId);
            return View(music);
        }

        // POST: Musics/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Author,genre,Price,Created,MusicstoreId")] Music music)
        {
            if (ModelState.IsValid)
            {
                db.Entry(music).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.MusicstoreId = new SelectList(db.Musicstores, "Id", "Name", music.MusicstoreId);
            return View(music);
        }

        // GET: Musics/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Music music = await db.Musics.FindAsync(id);
            if (music == null)
            {
                return HttpNotFound();
            }
            return View(music);
        }

        // POST: Musics/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Music music = await db.Musics.FindAsync(id);
            db.Musics.Remove(music);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
