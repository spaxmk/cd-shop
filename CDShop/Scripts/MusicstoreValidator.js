﻿function validation() {

    var nameElement = $("#name").val();
    var cityElement = $("#city").val();
    
    var flagName = true;
    var flagCity = true;

    if (nameElement.length < 5) {
        flagName = false;
    }
    if (cityElement.length < 4) {
        flagCity = false;
    }

    if (flagName) {
        hideDiv("#nameError");
    }
    else {
        showDiv("#nameError");
    }
    if (flagCity) {
        hideDiv("#cityError");
    }
    else {
        showDiv("#cityError");
    }
    if (flagName && flagCity) {
        $("#formCreate").submit();
    }
};

function showDiv(idDiv) {
    $(idDiv).removeClass("hidden");
    $(idDiv).addClass("text-danger");
};

function hideDiv(idDiv) {
    $(idDiv).removeClass("text-danger");
    $(idDiv).addClass("hidden");
};

$("#buttonPress").click(validation);