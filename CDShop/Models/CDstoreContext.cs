﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CDShop.Models
{
    public class CDstoreContext : DbContext
    {
        public DbSet<Music> Musics { get; set; }
        public DbSet<Musicstore> Musicstores { get; set; }

        public CDstoreContext() : base("name=CDstoreContext")
        {

        }

    }
}