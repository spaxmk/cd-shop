﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CDShop.Models
{
    public enum Genre
    {
        Blues,
        Classical,
        Electronic,
        Folk,
        Instrumental,
        Metal,
        HipHop,
        Jazz,
        Pop,
        Rock
    }


    public class Music
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Required]
        [StringLength(50)]
        public string Author { get; set; }
        [Display(Name ="Genre")]
        public Genre genre { get; set; }
        [Required]
        [Range(0, double.MaxValue)]
        [Display(Name = "RSD")]
        public double Price { get; set; }
        [Required]
        public DateTime Created { get; set; }
        [ForeignKey("Musicstore")]
        public int MusicstoreId { get; set; }
        public Musicstore Musicstore { get; set; }


    }
}