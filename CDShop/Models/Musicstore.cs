﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CDShop.Models
{
    
    public class Musicstore
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name = "Musicstore")]
        public string Name { get; set; }
        [Required]
        [StringLength(50)]
        public string City { get; set; }
        public List<Music> Musics { get; set; }
    }
}